<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Admin",
            'name_user' => "Administrador",
            'email' => Str::random(10) . '@gmail.com',
            'email_verified_at' => Carbon\Carbon::now(),
            'password' =>'Admin',
            'city_id' => 1,
            'type_profile_id' => 2
        ]);

    }
}
