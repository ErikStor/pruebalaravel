$(document).ready(function () {

    // validate signup form on keyup and submit
    $("#formCreate").validate({
        rules: {
            name: "required",
            name_user: "required",
            password: "required",
            confirmPassword: "required",
            email: "required",
            city: "required",
            name: {
                required: true,
                minlength: 1,
                maxlength: 191,
            },
            name_user: {
                required: true,
                minlength: 1,
                maxlength: 191,
            },
            password: {
                required: true,
                minlength: 5,
                maxlength: 191,
                equalTo: "#confirmPassword"
            },
            confirmPassword: {
                required: true,
                minlength: 5,
                maxlength: 191,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            city: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "El nombre es obligatorio",
                minlength: "El nombre debe contener m&iacute;nimo 1 caracter",
                maxlength: "El nombre no debe contener m&aacute;s 191 caracteres",
            },
            name_user: {
                required: "El nombre de usuario es obligatorio",
                minlength: "El nombre de usuario debe contener m&iacute;nimo 1 caracter",
                maxlength: "El nombre de usuario no debe contener m&aacute;s 191 caracteres",
            },
            password: {
                required: "La contraseña es obligatoria",
                minlength: "La contraseña debe contener m&iacute;nimo 5 caracteres",
                maxlength: "La contraseña no debe contener m&aacute;s de 191 caracteres",
                equalTo: "Las contraseñas no coinciden"
            },
            confirmPassword: {
                required: "La confirmaci&oacute;n de la contraseña es obligatoria",
                minlength: "La confirmaci&oacute;n de la contraseña debe contener m&iacute;nimo 5 caracteres",
                maxlength: "La confirmaci&oacute;n de la contraseña no debe contener m&aacute;s de 191 caracteres",
                equalTo: "Las contraseñas no coinciden"
            },
            city:{
                required: "La ciudad es obligatoria",
            },
            email: "Por favor ingrese un correo electr&oacute;nico valido"
        }
    });


});
