<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\User as UserModel;

class User extends Controller
{

    protected $rules = [
        'name' => 'required|min:3|max:191|regex:' . "/[a-zA-Z\s\w]/",
        'name_user' => 'required|min:3|max:191|regex:' . "/[a-zA-Z\s\w]/",
        'password' => 'required|min:5|max:191||regex:' . "/[a-zA-Z\s\w]/",
        'confirmPassword' => 'required|min:5|max:191|regex:' . "/[a-zA-Z\s\w]/",
        'email' => 'required|max:191|regex:' . "/[a-zA-Z]+@+[a-zA-Z]+.+[a-zA-Z]/",
        'city' => 'required|max:20|regex:' . "/^[1-9]+$/"
    ];

    protected $messages = [
        'required' => 'El campo :attribute es requerido',
        'max' => 'El campo :attribute no puede contener m&aacute;s de :max caracteres',
        'min' => 'El campo :attribute debe contener un m&iacute;nimo de :min caracteres',
        'regex' => 'El campo :attribute no tiene el formato correcto'
    ];

    public function create()
    {
        return view('user.user');
    }

    public function createProcess(Request $request)
    {

        $this->validate($request, $this->rules, $this->messages);


        $user = new UserModel();
        $user->name = $_POST["name"];
        $user->name_user = $_POST["name_user"];
        $user->email = $_POST["email"];
        //$user->password = encrypt($_POST["password"],false);
        $user->password = encrypt($_POST["password"],false);
        $user->city_id = $_POST["city"];

        $user->save();

    }

}
