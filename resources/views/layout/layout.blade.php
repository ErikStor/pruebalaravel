<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Crear usuario</title>
    <link href="../../css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="../../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="container">

            @yield('content')

    </div>

</div>
<footer>
    <script src="../../js/jquery-3.4.1.min.js"></script>
    <script src="../../js/jquery.validate.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/global.js"></script>
</footer>
</body>
</html>
