<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CityTableSeeder::class);
         $this->call(TypeProfileTableSeeder::class);
         $this->call(UserTableSeeder::class);
    }
}
