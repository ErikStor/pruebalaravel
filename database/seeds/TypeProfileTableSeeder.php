<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeProfileTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_profile')->insert([
            'name' => "Usuario"
        ]);

        DB::table('type_profile')->insert([
            'name' => "Administrador"
        ]);
    }
}
