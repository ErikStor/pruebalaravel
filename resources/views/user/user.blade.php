@extends('layout.layout')

@section('content')
    <form id="formCreate" method="post" action="{{route("createUser")}}">
        <fieldset>
            <legend>Registrar usuario</legend>

            <div class="col-12 form-group {{$errors->has('name') ? 'alert alert-danger':''}}">
                <label for="name">Nombre:</label>
                <input id="name" name="name" class="form-control" type="text" tabindex="1" minlength="1"
                       maxlength="191" placeholder="Nombre..." value="{{old('name')}}">

                @if($errors->has('name'))
                    <span class="help-block"><strong>{{$errors->first('name')}}</strong></span>
                @endif
            </div>


            <div class="col-12 form-group  {{$errors->has('name_user') ? 'alert alert-danger':''}}">
                <label for="name_user">Nombre de usuario:</label>
                <input id="name_user" name="name_user" class="form-control" type="text" tabindex="2" minlength="1"
                       maxlength="191" placeholder="Nombre de usuario..." value="{{old('name_user')}}">
                @if($errors->has('name_user'))
                    <span class="help-block"><strong>{{$errors->first('name_user')}}</strong></span>
                @endif
            </div>


            <div class="col-12 form-group {{$errors->has('confirmPassword') ? 'alert alert-danger':''}}">
                <div class="{{$errors->has('password') ? 'alert alert-danger':''}}">
                    <label for="password">Contraseña:</label>
                    <input id="password" name="password" class="form-control" type="password" tabindex="3"
                           minlength="5" maxlength="191" placeholder="Contraseña...">
                    @if($errors->has('password'))
                        <span class="help-block"><strong>{{$errors->first('password')}}</strong></span>
                    @endif
                </div>
                <div>
                    <label for="confirmPassword">Confirmaci&oacute;n contraseña:</label>
                    <input id="confirmPassword" name="confirmPassword" class="form-control" type="password"
                           tabindex="4" minlength="5" maxlength="191"
                           placeholder="Confirmaci&oacute;n contraseña...">
                    @if($errors->has('confirmPassword'))
                        <span class="help-block"><strong>{{$errors->first('confirmPassword')}}</strong></span>
                    @endif
                </div>
            </div>


            <div class="col-12 form-group  {{$errors->has('email') ? 'alert alert-danger':''}}">
                <div>
                    <label for="email">Correo electr&oacute;nico</label>
                    <input id="email" name="email" class="form-control" type="email" tabindex="5"
                           maxlength="191" placeholder="Correo electr&oacute;nico..." value="{{old('email')}}">
                    @if($errors->has('email'))
                        <span class="help-block"><strong>{{$errors->first('email')}}</strong></span>
                    @endif
                </div>
            </div>


            <div class="col-12 form-group  {{$errors->has('city') ? 'alert alert-danger':''}}">
                <div>
                    <label for="city">Ciudad</label>
                    <select id="city" name="city" class="form-control" tabindex="6">
                        <option value="1" selected>Cali</option>
                    </select>
                    @if($errors->has('city'))
                        <span class="help-block"><strong>{{$errors->first('city')}}</strong></span>
                    @endif
                </div>
            </div>


            <div class="col-12 d-flex justify-content-center">
                {{ csrf_field() }}
                <button type="submit" tabindex="7" class="btn btn-primary">Registrar</button>
            </div>
        </fieldset>
    </form>

@endsection
